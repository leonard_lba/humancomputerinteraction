﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class ClientUI : MonoBehaviour {

    static NetworkClient client;
    Vector2 startPosition = Vector2.zero;

    private void OnGUI()
    {
        IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
        string ipAddress = "";
        foreach(IPAddress ip in host.AddressList)
        {
            if(ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            {
                ipAddress = ip.ToString();
            }
        }

        GUI.Box(new Rect(10, Screen.height - 50, 100, 50), ipAddress);
        GUI.Label(new Rect(20, Screen.height - 30, 100, 20), "Status: " + client.isConnected);

        if (!client.isConnected)
        {
            if (GUI.Button(new Rect(10, 10, 60, 50), "Connect"))
            {
                Connect();
            }
        }
    }

    private void Connect()
    {
        //Die anzugebene IP-Addresse steht unten links im Fenster, wenn die Anwendung ausgeführt wird ;)
        client.Connect("192.168.178.36", 25000);
    }

    // Use this for initialization
    void Start () {
        client = new NetworkClient();
	}

    static public void SendRotationInfo(Vector2 x)
    {
        if (client.isConnected)
        {
            StringMessage msg = new StringMessage();
            msg.value = x.x.ToString() + "|" + x.y.ToString();
            client.Send(888, msg);
        }
    }
	
	// Update is called once per frame
	void Update () {
		if(Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if(touch.phase == TouchPhase.Began)
            {
                startPosition = touch.position;
            }
            if(touch.phase == TouchPhase.Moved)
            {
                SendRotationInfo(touch.position - startPosition);
            }
        }
	}
}
