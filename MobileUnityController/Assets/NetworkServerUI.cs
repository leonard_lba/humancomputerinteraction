﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class NetworkServerUI : MonoBehaviour {

    public GameObject player;

    string rotation1 = "";
    string rotation2 = "";
    string rotation3 = "";

    private void OnGUI()
    {
        IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
        string ipAddress = "";
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            {
                ipAddress = ip.ToString();
            }
        }
        //string ipAddress = Network.player.ipAddress;
        GUI.Box(new Rect(10, Screen.height - 50, 100, 50), ipAddress);
        GUI.Label(new Rect(20, Screen.height - 35, 100, 20), "Status: " + NetworkServer.active);
        GUI.Label(new Rect(20, Screen.height - 20, 100, 20), "Connected: " + NetworkServer.connections.Count);
        GUI.Box(new Rect(Screen.width / 2 - 200, Screen.height / 2 - 100, 400, 200), rotation1 + "\n" + rotation2 + "\n" + rotation3);


    }

    // Use this for initialization
    void Start()
    {
        NetworkServer.Listen(25000);
        NetworkServer.RegisterHandler(888, ServerReceiveMessage);
    }

    private void ServerReceiveMessage(NetworkMessage netMsg)
    {
        StringMessage msg = new StringMessage();
        msg.value = netMsg.ReadMessage<StringMessage>().value;

        string[] deltas = msg.value.Split('|');
        rotation1 = deltas[0] + "   " + deltas[1];
        player.transform.position += new Vector3(float.Parse(deltas[0]) / 1000, 0, float.Parse(deltas[1]) / 1000);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
